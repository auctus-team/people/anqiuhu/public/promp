import numpy as np
import matplotlib.pyplot as plt
import ProMP_discrete
import cv2


promp = ProMP_discrete.ProMP(n_promps=1, n_bfs=30)

num_trajectories = 50

trajectories1 = np.zeros((num_trajectories,100))
for i in range(num_trajectories):
    t = np.linspace(0, 1, 100 )
    trajectories1[i,:] = (0.5 * np.sin(2 * np.pi * t) + 0.05 * np.random.randn(t.size))


"""
#desired path from image
image = cv2.imread('/home/anqiu/ProMP/s.png', cv2.IMREAD_GRAYSCALE)
edges = cv2.Canny(image, threshold1=200, threshold2=200)
y_des1 = np.argwhere(edges == 255)
y_des1 = (y_des1-y_des1[0]).T #shift start point at (0,0)
"""


trajectories = promp.imitate_path(trajectories1)

promp.fit(trajectories)


new_trajectory = promp.sample_trajectory()
#print(np.max(new_trajectory))

std = promp.get_std()
print(std.shape)

plt.figure(figsize=(8, 4))


for i in range(trajectories.shape[1]):  
    plt.plot(trajectories[0, i, :], 'r', alpha=0.5, label='Original Trajectory' if i == 0 else "_nolegend_")
    
plt.xlabel("timesteps (dt=1ms )")
plt.ylabel("trajectory")   

plt.figure(figsize=(8, 4))
new_trajectory = promp.sample_trajectory()[0]  
plt.plot(new_trajectory, 'b', linewidth=2, label='Generated Trajectory')

time_steps = np.arange(new_trajectory.shape[0])
plt.fill_between(time_steps, new_trajectory - std[0], new_trajectory + std[0], color='blue', alpha=0.2, label='Confidence Interval')

plt.title("imitate path")
plt.xlabel("timesteps (dt=1ms )")
plt.ylabel("trajectory")
plt.legend()
plt.show()


