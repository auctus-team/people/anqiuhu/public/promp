import numpy as np
import matplotlib.pyplot as plt
import cv2

"""
1. imitate_path: interpolate the input dedsired trajectory to make sure it has the same length as timesteps
2. fit: learn weights from demonstration by maximum likelihood estimation
3. sample_trajectory: generate trajectory
"""

class ProMP:
    def __init__(self, n_promps, n_bfs, dt=0.001, run_time=1):
        self.n_promps = n_promps
        self.n_bfs = n_bfs
        self.dt = dt
        self.run_time = run_time
        self.width = 1/(self.n_bfs**2)
        self.centers = np.linspace(0, self.run_time, n_bfs)
        self.widths = np.ones(n_bfs) *self.width
        self.weights_mean = None
        self.weights_covariance = None
        
        self.timesteps = int(self.run_time / self.dt)
        
        
    def basis_function(self, x):
        return np.exp(-0.5 * ((x - self.centers) ** 2) / self.widths)
        
    """   
    def fit(self, trajectories, regularization_lambda=1e-3):
        self.weights_mean = []
        self.weights_covariance = []

        Phi = np.array([self.basis_function(t) for t in np.linspace(0, self.run_time, self.timesteps)])

        for d in range(self.n_promps):
            W = np.array([np.linalg.pinv(Phi).dot(trajectory) for trajectory in trajectories[d]])
            self.weights_mean.append(np.mean(W, axis=0))
            regularization_matrix = regularization_lambda * np.eye(W.shape[1])  
            self.weights_covariance.append(np.cov(W.T) + regularization_matrix)
        """    
            
    def fit(self, trajectories, regularization_lambda=1e-3):
    
        Phi = np.array([self.basis_function(t) for t in np.linspace(0, self.run_time, self.timesteps)])
        Phi_T_Phi = Phi.T.dot(Phi) + regularization_lambda * np.eye(self.n_bfs)  

    
        self.weights_mean = np.zeros((self.n_promps, self.n_bfs))
        self.weights_covariance = np.zeros((self.n_promps, self.n_bfs, self.n_bfs))

    
        for d in range(self.n_promps):
            W = np.array([np.linalg.solve(Phi_T_Phi, Phi.T.dot(trajectory)) for trajectory in trajectories[d]])
            
            self.weights_mean[d] = np.mean(W, axis=0)
            self.weights_covariance[d] = np.cov(W.T) + regularization_lambda * np.eye(self.n_bfs)
            print(self.weights_mean[d].shape)


   

    def sample_trajectory(self):
        sampled_trajectories = []
        Phi = np.array([self.basis_function(t) for t in np.linspace(0, self.run_time, self.timesteps)])
    
        for d in range(len(self.weights_mean)):
            sample_weights = np.random.multivariate_normal(self.weights_mean[d], self.weights_covariance[d])
            sampled_trajectories.append(Phi.dot(sample_weights))
    
        return np.array(sampled_trajectories)

      
    def imitate_path(self, y_des):
    # interpolate the input dedsired trajectory to make sure it has the same length as timesteps
        import scipy.interpolate
        y_des = np.array(y_des)

        if y_des.ndim == 2:
           y_des = y_des[np.newaxis, :]
    
    
        path = np.zeros((self.n_promps, y_des.shape[1], self.timesteps))
        x = np.linspace(0, self.run_time, y_des.shape[2])  
    
        for d in range(self.n_promps):
            for i in range(y_des.shape[1]):  
                path_gen = scipy.interpolate.interp1d(x, y_des[d, i], kind='linear', fill_value="extrapolate")
                for t in range(self.timesteps):
                    path[d, i, t] = path_gen(t * self.dt)
        return path
        
    
    def get_std(self):
   
        Phi = np.array([self.basis_function(t) for t in np.linspace(0, self.run_time, self.timesteps)])
        std_list = []

        for d in range(self.n_promps):
        
            variances = np.sum((Phi @ self.weights_covariance[d]) * Phi, axis=1)
            std = np.sqrt(variances)
            std_list.append(std)
        
        return np.array(std_list)




