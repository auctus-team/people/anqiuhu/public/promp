# Probabilistic Movement primitive(ProMP)
" $y_t \in \mathbb{R}^D$ is the recording of a robot’s joint space or certain Cartesian coordinates.

The coordinates $y_t$ are modelled as a linear combination $z_t(\omega) = \omega \phi_t$ under the presence of zero-mean Gaussian noise, 
that is, 

$y_t = z_t (\omega) + \epsilon_t , \epsilon_t ∼ \mathcal{N} (0, \sum_y)$ 

The vector $\phi_t$ consists of the values at time t of M basis functions. 

$\phi_t^i=exp(−(τ (t) − c_i)^2/(2h))$

As a result, the observation model is given by 

$p(y_t|\omega) = \mathcal{N} (y_t;\omega \phi_t, \sum_y)$

The distribution over weights p(w) is chosen to be Gaussian, i.e., $p(\omega) = \mathcal{N}(vect(\omega); vect(\mu_\omega), \sum_\omega)$"

(Frank et al., 2022, p. 3)

# 1D ProMP
desired path $= 0.5 \sin(2 \pi t) + 0.05 \mathcal{N} (0, 1)$

## 5 demonstration with 10 basis functions and 30 basis functions
<figure>
<img src="pictures/1D/ndemos=5,demo.png" alt="" width="33%">
<img src="pictures/1D/nbfs=10,ndemos=5,path.png" alt="" width="33%">
<img src="pictures/1D/nbfs=30,ndemos=5,path.png" alt="" width="33%">
</figure>

## 50 demonstration with 10 basis functions and 30 basis functions
<figure>
<img src="pictures/1D/ndemos=50,demo.png" alt="" width="33%">
<img src="pictures/1D/nbfs=10,ndemos=50,path.png" alt="" width="33%">
<img src="pictures/1D/nbfs=30,ndemos=50,path.png" alt="" width="33%">
</figure>

# 2D ProMP
## handwriting path with noise (10 basis functions, 30 basis functions,50 basis functions)
<figure>
<img src="pictures/2D/3,nbfs=10.png" alt="" width="33%">
<img src="pictures/2D/3,nbfs=30.png" alt="" width="33%">
<img src="pictures/2D/3,nbfs=50.png" alt="" width="33%">
</figure>

## handwriting path without noise (50 basis functions)
<figure>
<img src="pictures/2D/lasa0.png" alt="" width="33%">
<img src="pictures/2D/lasa1.png" alt="" width="33%">
<img src="pictures/2D/lasa2.png" alt="" width="33%">
<img src="pictures/2D/lasa3.png" alt="" width="33%">
<img src="pictures/2D/lasa4.png" alt="" width="33%">
<img src="pictures/2D/lasa5.png" alt="" width="33%">
<img src="pictures/2D/lasa6.png" alt="" width="33%">
<img src="pictures/2D/lasa7.png" alt="" width="33%">
<img src="pictures/2D/lasa8.png" alt="" width="33%">
</figure>
