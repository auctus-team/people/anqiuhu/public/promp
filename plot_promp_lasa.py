
import numpy as np
import matplotlib.pyplot as plt
import _lasa
import ProMP_discrete


plt.figure(figsize=(6, 4))
T, X, Xd, Xdd, dt, shape_name = _lasa.load_lasa(8)
promp = ProMP_discrete.ProMP(n_promps=X.shape[2],n_bfs=50)
X = np.transpose(X, axes=(2,0,1))
trajectories = promp.imitate_path(X)
promp.fit(trajectories)
new_trajectory = promp.sample_trajectory()
    
for i in range(trajectories.shape[1]):  
    plt.plot(trajectories[0, i, :],trajectories[1, i, :], 'r', alpha=0.5, label='Original Trajectory' if i == 0 else "_nolegend_")

plt.plot(new_trajectory[0],new_trajectory[1], 'b', linewidth=2, label='Generated Trajectory')
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.legend()
plt.show()

