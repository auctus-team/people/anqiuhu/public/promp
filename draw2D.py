import numpy as np
import matplotlib.pyplot as plt
import ProMP_discrete
import cv2


promp = ProMP_discrete.ProMP(n_promps=2, n_bfs=50)


#desired path from image(2D)
image_paths1 = [
    '/home/anqiu/ProMP/IMG_3008.PNG',
    '/home/anqiu/ProMP/IMG_3009.PNG',
    '/home/anqiu/ProMP/IMG_3010.PNG',
    
]


image_paths2 = [
    '/home/anqiu/ProMP/1.png',
    '/home/anqiu/ProMP/2.png',
    '/home/anqiu/ProMP/3.png',
    '/home/anqiu/ProMP/4.png',
]
processed_images = []

# Find the biggest shape of the images
max_size = 0
for path in image_paths1:
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    edges = cv2.Canny(image, threshold1=200, threshold2=200)
    y_des = np.argwhere(edges == 255)
    y_des = (y_des - y_des[0]).T
    if y_des.shape[1] > max_size:
        max_size = y_des.shape[1]

# Process all images and apply zero padding
for path in image_paths1:
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    edges = cv2.Canny(image, threshold1=200, threshold2=200)
    y_des = np.argwhere(edges == 255)
    y_des = (y_des - y_des[0]).T
    # Apply zero padding
    padded = np.pad(y_des, ((0, 0), (0, max_size - y_des.shape[1])), 'constant')
    processed_images.append(padded[None, :, :])

# Concatenate processed image data
final_result = np.concatenate(processed_images, axis=0)

# Transpose the final_result to get the desired shape (2, 4, n)
trajectories = np.transpose(final_result, axes=(1, 0, 2))
c=trajectories.shape[2]
trajectories = trajectories[:,:,:c-150]

print(trajectories.shape)

trajectories = promp.imitate_path(trajectories)
print(trajectories.shape)

promp.fit(trajectories)


new_trajectory = promp.sample_trajectory()
print(new_trajectory.shape)

plt.figure(figsize=(6, 4))


for i in range(trajectories.shape[1]):  
    plt.plot(trajectories[0, i, :],trajectories[1, i, :], 'r', alpha=0.5, label='Original Trajectory' if i == 0 else "_nolegend_")


 
plt.plot(new_trajectory[0],new_trajectory[1], 'b', linewidth=2, label='Generated Trajectory')

plt.legend()
plt.show()



















